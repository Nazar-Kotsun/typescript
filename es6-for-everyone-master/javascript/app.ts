import { getFighters } from './services/fightersService'
import { createFighters } from './fightersView';
import {Fighter} from "./helpers/Fighter";

const rootElement: HTMLElement | null = document.getElementById('root');
const loadingElement: HTMLElement | null = document.getElementById('loading-overlay');

export async function startApp() {
    try {
        (loadingElement as HTMLElement).style.visibility = 'visible';

        const fighters: Fighter[] = (await getFighters() as Fighter[]);
        const fightersElement: HTMLElement = createFighters(fighters);

        (rootElement as HTMLElement).appendChild(fightersElement);
    } catch (error) {
        console.warn(error);
        (rootElement as HTMLElement).innerText = 'Failed to load data';
    } finally {
        (loadingElement as HTMLElement).style.visibility = 'hidden';
    }
}
