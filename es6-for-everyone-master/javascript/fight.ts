import {Fighter} from "javascript/helpers/Fighter";

export function fight(firstFighter: Fighter, secondFighter: Fighter):Fighter {

    let attacker: Fighter;
    let blocker: Fighter;

    let randomRole: boolean = Math.random() >= 0.5;

    if(randomRole) {
        attacker = JSON.parse(JSON.stringify(firstFighter));
        blocker = JSON.parse(JSON.stringify(secondFighter));
    }
    else {
        attacker = JSON.parse(JSON.stringify(secondFighter));
        blocker = JSON.parse(JSON.stringify(firstFighter));
    }
    let new_attacker: Fighter;
    let new_blocker: Fighter;

    while (attacker.health >= 0 && blocker.health >= 0) {


        blocker.health -= getDamage(attacker, blocker);

        new_attacker = blocker;
        new_blocker = attacker;
        attacker = new_attacker;
        blocker = new_blocker;
    }

    return blocker;
}

export function getDamage(attacker: Fighter, enemy: Fighter): number {
    // damage = hit - block
    // return damage
    let damage: number = getHitPower(attacker) - getBlockPower(enemy);

    if(damage < 0){
        damage = 0;
    }

    return damage;
}

export function getHitPower(fighter: Fighter): number {
    // return hit power

    return fighter.attack * Math.random() * (2 - 1) + 1;
}

export function getBlockPower(fighter: Fighter): number {
    // return block power

    return fighter.defense * Math.random() * (2 - 1) + 1;
}
