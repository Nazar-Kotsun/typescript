import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import {getFighterDetails} from "./services/fightersService";
import {Fighter} from "./helpers/Fighter";

export function createFighters(fighters: Fighter[]): HTMLElement {

    const selectFighterForBattle = createFightersSelector();
    const fighterElements: HTMLElement[] = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
    const fightersContainer: HTMLElement = createElement({ tagName: 'div', className: 'fighters' });

    fightersContainer.append(...fighterElements);

    return fightersContainer;
}


async function showFighterDetails(event: Event, fighter: Fighter) {
    const fullInfo: Fighter = await getFighterInfo(fighter._id);
    showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: string) {

    return await getFighterDetails(fighterId);
}

function createFightersSelector() {

    const selectedFighters: Map<string, Fighter> = new Map<string, Fighter>();

    return async function selectFighterForBattle(event: Event, fighter: Fighter) {
        const fullInfo: Fighter = await getFighterInfo(fighter._id);

        if ((event.target as HTMLInputElement).checked) {
            selectedFighters.set(fighter._id, fullInfo);
        } else {
            selectedFighters.delete(fighter._id);
        }

        if (selectedFighters.size === 2) {

            let selectedFightersArray: Fighter[] = Array.from(selectedFighters.values());

            const winner: Fighter = fight(selectedFightersArray[0], selectedFightersArray[1]);
            showWinnerModal(winner);
        }
    }
}
