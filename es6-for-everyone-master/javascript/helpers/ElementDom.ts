import {IAttributes} from "./IAttributes";

export class ElementDom {

    tagName: string;
    className?: string;
    attributes?: IAttributes;

    constructor(tagName: string, className:string = "", attributes: IAttributes = {}) {

        this.tagName = tagName;
        this.className = className;
        this.attributes = attributes;
    }
}