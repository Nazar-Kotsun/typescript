export class Fighter {

    public _id: string;
    public name: string;
    public attack: number;
    public defense: number;
    public health: number;
    public source: string;

    constructor(_id: string, name: string, attack: number, defense: number, health: number, source: string) {

        this._id = _id;
        this.name = name;
        this.attack = attack;
        this.defense = defense;
        this.health = health;
        this.source = source;
    }

}