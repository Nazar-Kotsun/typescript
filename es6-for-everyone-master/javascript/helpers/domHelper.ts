import {ElementDom} from "./ElementDom";
import {IAttributes} from "./IAttributes";

export function createElement(elementDom: ElementDom) {
    const element = document.createElement(elementDom.tagName);

    if (elementDom.className) {
        element.classList.add(elementDom.className);
    }

    if(elementDom.attributes)
        Object.keys(elementDom.attributes as IAttributes).forEach(key => element.setAttribute(key, (elementDom.attributes as IAttributes)[key]));

    return element;
}


