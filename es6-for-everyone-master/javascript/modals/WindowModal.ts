export  class WindowModal {

    public title: string;
    public bodyElement: HTMLElement;

    constructor(title: string, bodyElement: HTMLElement) {
        this.title = title;
        this.bodyElement = bodyElement;
    }

}