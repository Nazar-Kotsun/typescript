import {createElement} from '../helpers/domHelper';
import { showModal } from './modal';
import {Fighter} from "../helpers/Fighter";
import {WindowModal} from "./WindowModal";

export  function showFighterDetailsModal(fighter: Fighter): void {

    const title: string = 'Fighter info';
    const bodyElement: HTMLElement = createFighterDetails(fighter);

    try {
        showModal({ title, bodyElement });
    }
    catch (error) {
        console.warn(error);
    }
}

function createFighterDetails(fighter: Fighter): HTMLElement {
    const { name, attack, defense, health, source }: Fighter = fighter;

    const fighterDetails: HTMLElement = createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement: HTMLElement = createElement({ tagName: 'span', className: 'fighter-name' });
    const attackElement: HTMLElement = createElement({tagName: 'span', className: 'fighter-attack'});
    const defenseElement: HTMLElement = createElement({tagName: 'span', className: 'fighter-defense'});
    const healthElement: HTMLElement = createElement({tagName: 'span', className: 'fighter-health'});
    const imageElement: HTMLElement = createElement({tagName: 'img', className: 'fighter-image', attributes: { src: source }});

    nameElement.innerText = "Name: " + name + '\n';
    attackElement.innerText = "Attack: " + attack + '\n';
    defenseElement.innerText = "Defense: " + defense + '\n';
    healthElement.innerText = "Health: " + health + '\n';

    fighterDetails.append(imageElement);
    fighterDetails.append(createElement({tagName: 'hr'}));
    fighterDetails.append(nameElement);
    fighterDetails.append(createElement({tagName: 'hr'}));
    fighterDetails.append(attackElement);
    fighterDetails.append(createElement({tagName: 'hr'}));
    fighterDetails.append(defenseElement);
    fighterDetails.append(createElement({tagName: 'hr'}));
    fighterDetails.append(healthElement);

    return fighterDetails;
}
