import { createElement } from '../helpers/domHelper';
import {WindowModal} from "./WindowModal";

export function showModal(windowModal: WindowModal): void {
    const root: HTMLElement | null = getModalContainer();

    const modal: HTMLElement = createModal(windowModal.title, windowModal.bodyElement);

    if(root == null){
        throw new Error("root wasn't found")
    }

    root?.append(modal);

}

function getModalContainer(): HTMLElement | null  {
    return document.getElementById('root');
}

function createModal(title:string, bodyElement:HTMLElement): HTMLElement {
    const layer: HTMLElement = createElement({ tagName: 'div', className: 'modal-layer' });
    const modalContainer: HTMLElement = createElement({ tagName: 'div', className: 'modal-root' });
    const header: HTMLElement = createHeader(title);
    //alert(bodyElement);
    modalContainer.append(header, bodyElement);
    layer.append(modalContainer);

    return layer;
}

function createHeader(title: string): HTMLElement {
    const headerElement: HTMLElement = createElement({ tagName: 'div', className: 'modal-header' });
    const titleElement: HTMLElement = createElement({ tagName: 'span'});
    const closeButton: HTMLElement = createElement({ tagName: 'div', className: 'close-btn' });

    titleElement.innerText = title;
    closeButton.innerText = '×';
    closeButton.addEventListener('click', hideModal);
    headerElement.append(title, closeButton);

    return headerElement;
}

function hideModal(event:Event): void {
    const modal = document.getElementsByClassName('modal-layer')[0];
    //alert(modal);
    modal?.remove();
}
