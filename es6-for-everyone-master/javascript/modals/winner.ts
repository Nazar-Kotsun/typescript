import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import {Fighter} from "../helpers/Fighter";


export  function showWinnerModal(fighter: Fighter): void {
    // show winner name and image
    const title: string = 'Winner!'
    const { name, source }: Fighter = fighter;

    const bodyElement: HTMLElement = createElement({ tagName: 'div', className: 'modal-body' });

    const nameElement: HTMLElement = createElement({ tagName: 'span', className: 'fighter-name' });
    const imageElement: HTMLElement = createElement({tagName: 'img', className: 'fighter-image', attributes: { src: source }});

    nameElement.innerText = "Name: " + name + '\n';
    bodyElement.append(imageElement);
    bodyElement.append(createElement({tagName: 'hr'}));
    bodyElement.append(nameElement);

    try {
        showModal({title, bodyElement});
    }
    catch (error) {
        console.warn(error);
    }
}

