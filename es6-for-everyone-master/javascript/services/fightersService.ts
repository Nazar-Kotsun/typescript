import { callApi } from '../helpers/apiHelper';
import {Fighter} from "../helpers/Fighter";

export async function getFighters() {
    try {
        const endpoint: string = 'fighters.json';
        const apiResult: Fighter[] = <Fighter[]>await callApi(endpoint, 'GET');

        return apiResult;
    } catch (error) {
        throw error;
    }
}

export async function getFighterDetails(id: string) {

    // endpoint - `details/fighter/${id}.json`;
    try {
        const endpoint: string = `details/fighter/${id}.json`;
        const apiResult: Fighter = <Fighter>await callApi(endpoint, 'GET')

        return apiResult;
    } catch (error) {
        throw error;
    }
}

