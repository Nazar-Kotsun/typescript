export function fight(firstFighter, secondFighter) {
    let attacker;
    let blocker;
    let randomRole = Math.random() >= 0.5;
    if (randomRole) {
        attacker = JSON.parse(JSON.stringify(firstFighter));
        blocker = JSON.parse(JSON.stringify(secondFighter));
    }
    else {
        attacker = JSON.parse(JSON.stringify(secondFighter));
        blocker = JSON.parse(JSON.stringify(firstFighter));
    }
    let new_attacker;
    let new_blocker;
    while (attacker.health >= 0 && blocker.health >= 0) {
        blocker.health -= getDamage(attacker, blocker);
        new_attacker = blocker;
        new_blocker = attacker;
        attacker = new_attacker;
        blocker = new_blocker;
    }
    return blocker;
}
export function getDamage(attacker, enemy) {
    // damage = hit - block
    // return damage
    let damage = getHitPower(attacker) - getBlockPower(enemy);
    if (damage < 0) {
        damage = 0;
    }
    return damage;
}
export function getHitPower(fighter) {
    // return hit power
    return fighter.attack * Math.random() * (2 - 1) + 1;
}
export function getBlockPower(fighter) {
    // return block power
    return fighter.defense * Math.random() * (2 - 1) + 1;
}
//# sourceMappingURL=fight.js.map