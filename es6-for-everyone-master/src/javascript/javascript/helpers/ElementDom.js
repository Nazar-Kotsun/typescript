export class ElementDom {
    constructor(tagName, className = "", attributes = {}) {
        this.tagName = tagName;
        this.className = className;
        this.attributes = attributes;
    }
}
//# sourceMappingURL=ElementDom.js.map