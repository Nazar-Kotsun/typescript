export class Fighter {
    constructor(_id, name, attack, defense, health, source) {
        this._id = _id;
        this.name = name;
        this.attack = attack;
        this.defense = defense;
        this.health = health;
        this.source = source;
    }
}
//# sourceMappingURL=Fighter.js.map