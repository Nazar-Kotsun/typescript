export function createElement(elementDom) {
    const element = document.createElement(elementDom.tagName);
    if (elementDom.className) {
        element.classList.add(elementDom.className);
    }
    if (elementDom.attributes)
        Object.keys(elementDom.attributes).forEach(key => element.setAttribute(key, elementDom.attributes[key]));
    return element;
}
//# sourceMappingURL=domHelper.js.map