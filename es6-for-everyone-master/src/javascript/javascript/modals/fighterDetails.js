import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
export function showFighterDetailsModal(fighter) {
    const title = 'Fighter info';
    const bodyElement = createFighterDetails(fighter);
    try {
        showModal({ title, bodyElement });
    }
    catch (error) {
        console.warn(error);
    }
}
function createFighterDetails(fighter) {
    const { name, attack, defense, health, source } = fighter;
    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
    const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
    const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
    const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
    const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: { src: source } });
    nameElement.innerText = "Name: " + name + '\n';
    attackElement.innerText = "Attack: " + attack + '\n';
    defenseElement.innerText = "Defense: " + defense + '\n';
    healthElement.innerText = "Health: " + health + '\n';
    fighterDetails.append(imageElement);
    fighterDetails.append(createElement({ tagName: 'hr' }));
    fighterDetails.append(nameElement);
    fighterDetails.append(createElement({ tagName: 'hr' }));
    fighterDetails.append(attackElement);
    fighterDetails.append(createElement({ tagName: 'hr' }));
    fighterDetails.append(defenseElement);
    fighterDetails.append(createElement({ tagName: 'hr' }));
    fighterDetails.append(healthElement);
    return fighterDetails;
}
//# sourceMappingURL=fighterDetails.js.map