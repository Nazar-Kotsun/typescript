import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
export function showWinnerModal(fighter) {
    // show winner name and image
    const title = 'Winner!';
    const { name, source } = fighter;
    const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
    const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: { src: source } });
    nameElement.innerText = "Name: " + name + '\n';
    bodyElement.append(imageElement);
    bodyElement.append(createElement({ tagName: 'hr' }));
    bodyElement.append(nameElement);
    try {
        showModal({ title, bodyElement });
    }
    catch (error) {
        console.warn(error);
    }
}
//# sourceMappingURL=winner.js.map